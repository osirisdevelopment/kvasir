import * as crypto from "crypto";

export function copy_to_clipboard(string) {
    const handler = (event) => {
        event.clipboardData.setData("text/plain", string);
        event.preventDefault();

        document.removeEventListener("copy", handler, true);
    }

    document.addEventListener("copy", handler, true);
    document.execCommand("copy");
}

const CHARS_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
const CHARS_CAPITALS = CHARS_LOWERCASE.toUpperCase();
const CHARS_NUMBERS = "1234567890";
const CHARS_SYMBOLS = "!@#$%&";
const CHARS_MORE_SYMBOLS = "^*()-_=+/.,><{}[]~|";

export function generate_password(length, options) {
    let chars = "";
    chars = chars + ((options.alpha) ? CHARS_CAPITALS + CHARS_LOWERCASE : "");
    chars = chars + ((options.numeric) ? CHARS_NUMBERS : "");
    chars = chars + ((options.symbols) ? CHARS_SYMBOLS : "");
    chars = chars + ((options.more_symbols) ? CHARS_MORE_SYMBOLS : "");

    if (length < 6) {
        return ""
    }

    let password = "";
    while (!valid_password(password, options)) {
        password = "";
        
        const values = new Uint8Array(length);
        crypto.randomFillSync(values);
        values.forEach((value) => {
            password = password + chars[value % chars.length];
        });
    }

    return password;
}

function valid_password(password, options) {
    const REGEX_ALPHA = new RegExp(`[${CHARS_LOWERCASE}${CHARS_CAPITALS}]`);
    const REGEX_NUMBERS = new RegExp(`[${CHARS_NUMBERS}]`);
    const REGEX_SYMBOLS = new RegExp(`[${CHARS_SYMBOLS}]`);
    const REGEX_MORE_SYMBOLS = new RegExp(`[${CHARS_MORE_SYMBOLS}]`);

    if (password.length < 6) {
        return false;
    }

    if (options.alpha && !REGEX_ALPHA.test(password)) {
        return false;
    }

    if (options.numeric && !REGEX_NUMBERS.test(password)) {
        return false;
    }

    if (options.symbols && !REGEX_SYMBOLS.test(password)) {
        return false;
    }

    if (options.more_symbols && !REGEX_MORE_SYMBOLS.test(password)) {
        return false;
    }

    return true;
}