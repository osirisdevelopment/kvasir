import "@/bulma/css/bulma.css";
import "@fortawesome/fontawesome";
import "@fortawesome/fontawesome-free-solid";
import Vue from "@/vue/dist/vue";
import App from "#/App";
import Kvasir from "#/Kvasir";
import "file-loader?name=[name].[ext]!#/images/ksavir64.png"
import "file-loader?name=[name].[ext]!#/images/ksavir.png"

let app = new Kvasir();
app.init();

new Vue({
    el: '#app',
    components: { App },
    data: function() {
        return {
            app: app
        }
    },
    template: '<App :app="app" />'
});
